#!/usr/bin/python

import sys
from typing import List

ARCHS = ['x86_64', 'noarch']
EL = ['el7', 'centos', 'rhel7u2', 'el7_2',
      'el6', 'el5', 'el6_5', 'el6_5', 'el6_6',
      'el6_7', 'el7_3', 'el7_3', 'sl6', 'rhel6u7']


def main():
    old_pkg_file = check_arg(sys.argv[1])
    new_pkg_file = check_arg(sys.argv[2])

    old = read_file(old_pkg_file)
    new = read_file(new_pkg_file)

    old_formatted = format(old)
    new_formatted = format(new)

    new, updated, downgraded, deleted, same = compare(old_formatted,
                                                      new_formatted)

    new.sort()
    updated.sort()
    deleted.sort()
    same.sort()

    print('== NEW: ' + str(len(new)))
    for item in new:
        print(item[0])

    print()
    print('== UPDATED: ' + str(len(updated)))
    for old_item, new_item in updated:
        print(old_item[0] + '  ->  ' + new_item[0])

    print()
    print('== DOWNGRADED: ' + str(len(downgraded)))
    for old_item, new_item in downgraded:
        print(old_item[0] + '  ->  ' + new_item[0])

    print()
    print('== DELETED: ' + str(len(deleted)))
    for item in deleted:
        print(item[0])

    print()
    print('== SAME: ' + str(len(same)))
    for item in same:
        print(item[0])


def check_arg(arg):
    if arg is None or arg == '':
        raise Exception(f'Argument {arg} is empty.')
    return arg


def read_file(file):
    with open(file) as f:
        lines = f.readlines()
        if len(lines) == 0:
            raise ValueError(f'File {file} is empty.')
        return lines


def format(lines: List[str]):
    # Remove unnecessary letters and
    # separate string on 5 parts (original_str, name, version, el, arch)
    formatted_lines = []
    for line in lines:

        if line == '':
            continue

        # Remove first number in strings like [0:dsfascsd]
        if ':' in line:
            line = line.split(':')[1]

        # Remove '\n' from the end
        line = line.rstrip()

        name_and_version, el, arch = separate_el_and_arch(line)

        version = name_and_version[1:]

        # Separate version and build for each item
        unpacked_version=[]
        for i in range(len(version)):
            if '-' in version[i]:
                for val in version[i].split('-'):
                    unpacked_version.append(val)
            else:
                unpacked_version.append(version[i])

        # Separate name and part of version
        name_and_ver = name_and_version[0]

        name = ''
        for i in range(len(name_and_ver) - 1, 0, -1):
            if name_and_ver[i].isdigit() and name_and_ver[i - 1] == '-':
                name = name_and_ver[0:i - 1]
                unpacked_version.insert(0, name_and_ver[i:])
                break

        # Finally we have tuple
        # ('package-name-0.2.1-4.el7.x86_64', 'package-name',
        # ['0', '2', '1', '4'], 'el7, 'x86_64')
        formatted_lines.append((line, name, unpacked_version, el, arch))

    return formatted_lines


def separate_el_and_arch(line):
    """
    Input line have format [package-name-0.2.1-4.el7.x86_64]
    
    1. Separate pkg by dots to list.
    2. From last to first check that substring is architecture or el
    3. Remove el and arch from splitted list
    4. Return tuple with format (['package-name-0','2','1-4'], 'el7', 'x86_64')
    
    """
    splitted_line = line.split('.')

    arch = ''
    el = ''

    remove_from_tail = 0
    reversed_splitted_line = list(reversed(splitted_line))
    length = len(reversed_splitted_line)
    range_iterations = length if length < 4 else 4
    for i in range(range_iterations):
        if reversed_splitted_line[i] in ARCHS:
            arch = reversed_splitted_line[i]
            remove_from_tail += 1

        if reversed_splitted_line[i] in EL:
            if el == '':
                el = reversed_splitted_line[i]
            else:
                el = reversed_splitted_line[i] + '.' + el
            remove_from_tail += 1

    # Something like [raidix-release-4]
    name_and_version = splitted_line[
                       0:len(splitted_line) - remove_from_tail]

    return (name_and_version, el, arch)


def compare(old, new):
    """
    1. Compare by name. If not equals, compare next element, 
        else if versions are equals, move to [same] list, 
        else move both to updated.
    2. Finally we have 4 lists: new, updated, deleted and same
    
    *move mean remove from original list and copy to another
    """
    updated_pkgs = []
    downgraded_pkgs = []
    same_pkgs = []
    old_to_delete = []
    for old_i in range(len(old) - 1, -1, -1):
        for new_i in range(len(new) - 1, -1, -1):

            # Name equality
            if old[old_i][1] == new[new_i][1]:
                # Version equality
                if old[old_i][2] == new[new_i][2]:
                    same_pkgs.append(new[new_i])
                else:
                    if is_version_increased(old[old_i][2], new[new_i][2]):
                        updated_pkgs.append((old[old_i], new[new_i]))
                    else:
                        downgraded_pkgs.append((old[old_i], new[new_i]))

                # Remove both, cause pkg is updated/same/downgraded
                del new[new_i]
                old_to_delete.append(old_i)

            else:
                continue

    deleted_pkgs = remove_indices_from_list(old, old_to_delete)

    return new, updated_pkgs, downgraded_pkgs, deleted_pkgs, same_pkgs


def is_version_increased(old_ver, new_ver):
    old_ver_len = len(old_ver)
    new_ver_len = len(new_ver)

    # If length is various, then package is updated
    if old_ver_len != new_ver_len:
        return True

    # Identically versions should not be on input but if it happens,
    # then package is updated
    if old_ver == new_ver:
        return True

    for i in range(old_ver_len):

        if old_ver[i].isdigit() and new_ver[i].isdigit():
            # Compare integers
            old_val = int(old_ver[i])
            new_val = int(new_ver[i])
            if old_val < new_val:
                break
            if old_val == new_val:
                continue
            if old_val > new_val:
                return False
        else:
            # Or compare lexicographically
            if old_ver[i] < new_ver[i]:
                break
            if old_ver[i] == new_ver[i]:
                continue
            if old_ver[i] > new_ver[i]:
                return False

    return True


def remove_indices_from_list(elements: List, indices):
    for i in remove_duplicates(indices):
        del elements[i]
    return elements


def remove_duplicates(seq, idfun=None):
    # Order preserving
    if idfun is None:
        def idfun(x): return x
    seen = {}
    result = []
    for item in seq:
        marker = idfun(item)
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    return result


if __name__ == "__main__":
    main()
